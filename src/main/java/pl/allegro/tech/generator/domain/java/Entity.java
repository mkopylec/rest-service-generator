package pl.allegro.tech.generator.domain.java;

import org.stringtemplate.v4.ST;

import java.util.Set;

import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public class Entity extends JavaClass {

    private final ClassProperty primaryKey;
    private final Set<ClassProperty> properties;
    private final boolean noArgConstructor;
    private final boolean allPropsConstructor;
    private final boolean equalsAndHashCode;
    private final boolean toString;

    Entity(String subPackage, String name, ClassProperty primaryKey, Set<ClassProperty> properties, boolean noArgConstructor, boolean allPropsConstructor, boolean equalsAndHashCode, boolean toString) {
        super(subPackage, name, "entity");
        failIfNull(primaryKey, "Entity primary key is null");
        this.primaryKey = primaryKey;
        this.properties = properties;
        this.noArgConstructor = noArgConstructor;
        this.allPropsConstructor = allPropsConstructor;
        this.equalsAndHashCode = equalsAndHashCode;
        this.toString = toString;
        addImports();
    }

    @Override
    protected void addAttributes(ST template) {
        template
                .add("properties", properties)
                .add("primaryKey", primaryKey)
                .add("noArgConstructor", noArgConstructor)
                .add("allPropsConstructor", allPropsConstructor)
                .add("equalsAndHashCode", equalsAndHashCode)
                .add("toString", toString);
    }

    public ClassProperty getPrimaryKey() {
        return primaryKey;
    }

    public Set<ClassProperty> getProperties() {
        return properties;
    }

    public boolean hasNoArgConstructor() {
        return noArgConstructor;
    }

    public boolean hasAllPropsConstructor() {
        return allPropsConstructor;
    }

    public boolean hasEqualsAndHashCode() {
        return equalsAndHashCode;
    }

    public boolean hasToString() {
        return toString;
    }

    private void addImports() {
        if (equalsAndHashCode) {
            addImport("org.apache.commons.lang3.builder.EqualsBuilder");
            addImport("org.apache.commons.lang3.builder.HashCodeBuilder");
        }
        if (toString) {
            addImport("org.apache.commons.lang3.builder.ToStringBuilder");
        }
        for (ClassProperty property : properties) {
            addImports(property.getImportsToAdd());
        }
    }
}
