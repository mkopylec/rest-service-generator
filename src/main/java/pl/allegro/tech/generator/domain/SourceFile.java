package pl.allegro.tech.generator.domain;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.misc.ErrorBuffer;

import java.io.File;
import java.io.IOException;

import static pl.allegro.tech.generator.template.TemplateProvider.getTemplate;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public abstract class SourceFile {

    private String projectDir;
    private final String templateName;
    private final SourceFileType fileType;

    protected SourceFile(String templateName, SourceFileType fileType) {
        failIfBlank(templateName, "Source file template name is empty");
        failIfNull(fileType, "Source file type is null");
        this.templateName = templateName;
        this.fileType = fileType;
    }

    public final void createSourceFile() throws IOException {
        ST template = getTemplate(templateName, fileType);
        fillTemplate(template);
        File outputFile = new File(getAbsoluteFilePath());
        outputFile.getParentFile().mkdirs();
        template.write(outputFile, new ErrorBuffer());
    }

    public String getProjectDir() {
        return projectDir;
    }

    public void setProjectDir(String projectDir) {
        failIfBlank(projectDir, "Project directory is empty");
        this.projectDir = projectDir;
    }

    public String getAbsoluteFilePath() {
        failIfBlank(projectDir, "Project directory is empty");
        return projectDir + getRelativeFilePath();
    }

    protected abstract void fillTemplate(ST template);

    protected abstract String getRelativeFilePath();
}
