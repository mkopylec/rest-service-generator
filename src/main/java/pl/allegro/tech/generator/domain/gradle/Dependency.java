package pl.allegro.tech.generator.domain.gradle;

import com.google.common.base.Objects;

import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;

public class Dependency {

    private String group;
    private String name;
    private String version;

    public Dependency(String group, String name, String version) {
        failIfBlank(group, "Dependency group is empty");
        failIfBlank(name, "Dependency name is empty");
        failIfBlank(version, "Dependency version is empty");
        this.group = group;
        this.name = name;
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dependency that = (Dependency) o;

        return Objects.equal(this.group, that.group) &&
                Objects.equal(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(group, name);
    }
}
