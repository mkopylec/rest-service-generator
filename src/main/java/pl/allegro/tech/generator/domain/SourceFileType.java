package pl.allegro.tech.generator.domain;

public enum SourceFileType {

    JAVA("templates/java", "/src/main/java", ".java"),
    GRADLE("templates/gradle", "/", ".gradle"),
    MISC("templates/misc", "/", null),
    CONFIG("templates/config", "/src/main/resources/config", ".properties");

    private final String templatesDir;
    private final String sourceFilesDir;
    private final String fileExtension;

    private SourceFileType(String templatesDir, String sourceFilesDir, String fileExtension) {
        this.templatesDir = templatesDir;
        this.sourceFilesDir = sourceFilesDir;
        this.fileExtension = fileExtension;
    }

    public String getTemplatesDir() {
        return templatesDir;
    }

    public String getSourceFilesDir() {
        return sourceFilesDir;
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
