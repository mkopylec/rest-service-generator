package pl.allegro.tech.generator.domain.java;

import java.util.HashSet;
import java.util.Set;

import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public class EntityBuilder {

    private String subPackage;
    private String name;
    private ClassProperty primaryKey;
    private Set<ClassProperty> properties = new HashSet<>();
    private boolean noArgConstructor;
    private boolean allPropsConstructor;
    private boolean equalsAndHashCode;
    private boolean toString;

    private EntityBuilder() {
    }

    public static EntityBuilder newEntity() {
        return new EntityBuilder();
    }

    public EntityBuilder withSubPackage(String subPackage) {
        this.subPackage = subPackage;
        return this;
    }

    public EntityBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public EntityBuilder withProperty(ClassProperty property) {
        failIfNull(property, "Entity property is null");
        properties.add(property);
        return this;
    }

    public EntityBuilder withPrimaryKey(ClassProperty primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }

    public EntityBuilder withNoArgConstructor(boolean noArgConstructor) {
        this.noArgConstructor = noArgConstructor;
        return this;
    }

    public EntityBuilder withAllPropsConstructor(boolean allPropsConstructor) {
        this.allPropsConstructor = allPropsConstructor;
        return this;
    }

    public EntityBuilder withEqualsAndHashCode(boolean equalsAndHashCode) {
        this.equalsAndHashCode = equalsAndHashCode;
        return this;
    }

    public EntityBuilder withToString(boolean toString) {
        this.toString = toString;
        return this;
    }

    public Entity build() {
        Entity entity = new Entity(subPackage, name, primaryKey, properties, noArgConstructor, allPropsConstructor, equalsAndHashCode, toString);
        return entity;
    }
}
