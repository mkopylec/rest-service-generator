package pl.allegro.tech.generator.domain.java;

import org.stringtemplate.v4.ST;

import static pl.allegro.tech.generator.utils.PrimitivesWrapper.getWrapper;

public class Repository extends JavaClass {

    private final Entity entity;

    public Repository(String subPackage, Entity entity) {
        super(subPackage, entity.getName() + "Repository", "repository");
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    @Override
    protected void addAttributes(ST template) {
        ClassProperty primaryKey = entity.getPrimaryKey();
        template
                .add("entityType", entity.getName())
                .add("primaryKeyType", primaryKey.isPrimitive() ? getWrapper(primaryKey.getDisplayedType()) : primaryKey.getDisplayedType());
    }

    @Override
    public void setRootPackage(String rootPackage) {
        super.setRootPackage(rootPackage);
        addImports();
    }

    private void addImports() {
        addImports(entity.getPrimaryKey().getImportsToAdd());
        if (!getFullPackage().equals(entity.getFullPackage())) {
            addImport(entity.getFullPackage() + "." + entity.getName());
        }
    }
}
