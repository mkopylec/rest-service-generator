package pl.allegro.tech.generator.domain.config;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.stringtemplate.v4.ST;
import pl.allegro.tech.generator.domain.SourceFile;

import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pl.allegro.tech.generator.domain.SourceFileType.CONFIG;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;

public class ApplicationConfig extends SourceFile {

    private final String profile;
    private String projectName;
    private final Set<Property> properties = new HashSet<>();

    public ApplicationConfig() {
        super("profile", CONFIG);
        profile = null;
    }

    public ApplicationConfig(String profile) {
        super("profile", CONFIG);
        failIfBlank(profile, "Application config profile is empty");
        this.profile = profile;
    }

    @Override
    protected void fillTemplate(ST template) {
        template
                .add("properties", properties)
                .add("profile", profile)
                .add("projectName", projectName);
    }

    @Override
    protected String getRelativeFilePath() {
        return new StringBuilder()
                .append(CONFIG.getSourceFilesDir())
                .append("/")
                .append("application" + (isBlank(profile) ? "" : "-" + profile))
                .append(CONFIG.getFileExtension())
                .toString();
    }

    public String getProfile() {
        return profile;
    }

    public void setProjectName(String projectName) {
        failIfBlank(projectName, "Project name is empty");
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void addProperty(Property property) {
        properties.add(property);
    }

    public Set<Property> getProperties() {
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ApplicationConfig rhs = (ApplicationConfig) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.profile, rhs.profile)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(profile)
                .toHashCode();
    }
}
