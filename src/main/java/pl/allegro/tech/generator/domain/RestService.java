package pl.allegro.tech.generator.domain;

import pl.allegro.tech.generator.domain.config.ApplicationConfig;
import pl.allegro.tech.generator.domain.gradle.BuildFile;
import pl.allegro.tech.generator.domain.gradle.SettingsFile;
import pl.allegro.tech.generator.domain.java.Entity;
import pl.allegro.tech.generator.domain.java.Main;
import pl.allegro.tech.generator.domain.java.Repository;
import pl.allegro.tech.generator.domain.misc.GitIgnore;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import static org.apache.commons.io.FileUtils.deleteDirectory;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public class RestService {

    private final String projectDir;
    private final String projectName;
    private final String rootPackage;
    private final BuildFile buildFile;
    private final SettingsFile settingsFile;
    private final Set<Entity> entities;
    private final Set<Repository> repositories;
    private final Set<ApplicationConfig> applicationConfigs;
    private final Main main;
    private final GitIgnore gitIgnore;

    RestService(String projectDir, String projectName, String rootPackage, BuildFile buildFile, SettingsFile settingsFile, Set<Entity> entities, Set<Repository> repositories, Set<ApplicationConfig> applicationConfigs, Main main, GitIgnore gitIgnore) {
        failIfBlank(projectDir, "REST service project directory is empty");
        failIfBlank(projectName, "REST service project name is empty");
        failIfBlank(rootPackage, "REST service root package is empty");
        failIfNull(buildFile, "REST service build file is null");
        failIfNull(entities, "REST service entities are null");
        failIfNull(entities, "REST service application configs are null");
        failIfNull(main, "REST service main class is null");
        failIfNull(gitIgnore, "REST service .gitignore file is null");
        this.settingsFile = settingsFile;
        this.main = main;
        this.projectDir = projectDir;
        this.projectName = projectName;
        this.rootPackage = rootPackage;
        this.buildFile = buildFile;
        this.entities = entities;
        this.repositories = repositories;
        this.applicationConfigs = applicationConfigs;
        this.gitIgnore = gitIgnore;
    }

    public void createProject() throws IOException {
        deleteDirectory(new File(projectDir));
        for (Entity entity : entities) {
            entity.createSourceFile();
        }
        for (Repository repository : repositories) {
            repository.createSourceFile();
        }
        for (ApplicationConfig config : applicationConfigs) {
            config.createSourceFile();
        }
        buildFile.createSourceFile();
        settingsFile.createSourceFile();
        main.createSourceFile();
        gitIgnore.createSourceFile();
    }

    public Main getMain() {
        return main;
    }

    public String getProjectDir() {
        return projectDir;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getRootPackage() {
        return rootPackage;
    }

    public SettingsFile getSettingsFile() {
        return settingsFile;
    }

    public BuildFile getBuildFile() {
        return buildFile;
    }

    public Set<Entity> getEntities() {
        return entities;
    }

    public Set<Repository> getRepositories() {
        return repositories;
    }

    public Set<ApplicationConfig> getApplicationConfigs() {
        return applicationConfigs;
    }

    public GitIgnore getGitIgnore() {
        return gitIgnore;
    }
}
