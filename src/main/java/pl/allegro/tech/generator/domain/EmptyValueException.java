package pl.allegro.tech.generator.domain;

public class EmptyValueException extends RuntimeException {

    public EmptyValueException(String message) {
        super(message);
    }
}
