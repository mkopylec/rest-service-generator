package pl.allegro.tech.generator.domain.gradle;

import java.util.HashSet;
import java.util.Set;

import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public class BuildFileBuilder {

    private String gradleVersion;
    private String javaVersion;
    private String mainClass;
    private String artifactGroup;
    private Set<Dependency> compileDependencies = new HashSet<>();
    private Set<Dependency> testDependencies = new HashSet<>();

    private BuildFileBuilder() {
    }

    public static BuildFileBuilder newBuildFile() {
        return new BuildFileBuilder();
    }

    public BuildFileBuilder withGradleVersion(String gradleVersion) {
        this.gradleVersion = gradleVersion;
        return this;
    }

    public BuildFileBuilder withJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
        return this;
    }

    public BuildFileBuilder withMainClass(String mainClass) {
        this.mainClass = mainClass;
        return this;
    }

    public BuildFileBuilder withArtifactGroup(String artifactGroup) {
        this.artifactGroup = artifactGroup;
        return this;
    }

    public BuildFileBuilder withCompileDependency(Dependency compileDependency) {
        failIfNull(compileDependency, "Build file compile dependency is null");
        compileDependencies.add(compileDependency);
        return this;
    }

    public BuildFileBuilder withTestDependency(Dependency testDependency) {
        failIfNull(testDependency, "Build file test dependency is null");
        testDependencies.add(testDependency);
        return this;
    }

    public BuildFile build() {
        BuildFile buildFile = new BuildFile(gradleVersion, javaVersion, mainClass, artifactGroup, compileDependencies, testDependencies);
        return buildFile;
    }
}
