package pl.allegro.tech.generator.domain;

import pl.allegro.tech.generator.domain.config.ApplicationConfig;
import pl.allegro.tech.generator.domain.gradle.BuildFile;
import pl.allegro.tech.generator.domain.gradle.Dependency;
import pl.allegro.tech.generator.domain.gradle.SettingsFile;
import pl.allegro.tech.generator.domain.java.Entity;
import pl.allegro.tech.generator.domain.java.Main;
import pl.allegro.tech.generator.domain.java.Repository;
import pl.allegro.tech.generator.domain.misc.GitIgnore;

import java.util.HashSet;
import java.util.Set;

import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

public class RestServiceBuilder {

    private String projectDir;
    private String projectName;
    private String rootPackage;
    private BuildFile buildFile;
    private SettingsFile settingsFile;
    private Set<Entity> entities = new HashSet<>();
    private Set<Repository> repositories = new HashSet<>();
    private Set<ApplicationConfig> applicationConfigs = new HashSet<>();
    private Main main;
    private GitIgnore gitIgnore;

    private RestServiceBuilder() {
    }

    public static RestServiceBuilder newRestService(String projectDir, String projectName, String rootPackage, BuildFile buildFile) {
        RestServiceBuilder builder = new RestServiceBuilder();
        builder.setProjectDir(projectDir);
        builder.setRootPackage(rootPackage);
        GitIgnore gitIgnore = new GitIgnore();
        gitIgnore.setProjectDir(projectDir);
        builder.setGitIgnore(gitIgnore);
        failIfNull(buildFile, "Build file is null");
        buildFile.setProjectDir(projectDir);
        builder.setBuildFile(buildFile);
        SettingsFile settingsFile = new SettingsFile(projectName);
        settingsFile.setProjectDir(projectDir);
        builder.setSettingsFile(settingsFile);
        builder.setProjectName(projectName);
        return builder;
    }

    public RestServiceBuilder withEntity(Entity entity) {
        failIfNull(entity, "Entity is null");
        entity.setProjectDir(projectDir);
        entity.setRootPackage(rootPackage);
        entities.add(entity);
        buildFile.getCompileDependencies().add(new Dependency("org.springframework.data", "spring-data-commons", "+"));
        if (entity.hasEqualsAndHashCode() || entity.hasToString()) {
            buildFile.getCompileDependencies().add(new Dependency("org.apache.commons", "commons-lang3", "+"));
        }
        return this;
    }

    public RestServiceBuilder withRepository(Repository repository) {
        failIfNull(repository, "Entity is null");
        repository.setProjectDir(projectDir);
        repository.setRootPackage(rootPackage);
        repositories.add(repository);
        return this;
    }

    public RestServiceBuilder withApplicationConfig(ApplicationConfig applicationConfig) {
        failIfNull(applicationConfig, "Application config is null");
        applicationConfig.setProjectDir(projectDir);
        applicationConfig.setProjectName(projectName);
        applicationConfigs.add(applicationConfig);
        return this;
    }

    public RestServiceBuilder withMainClass(Main main) {
        main.setProjectDir(projectDir);
        main.setRootPackage(rootPackage);
        this.main = main;
        return this;
    }

    public RestService build() {
        RestService restService = new RestService(
                projectDir, projectName, rootPackage, buildFile, settingsFile, entities, repositories, applicationConfigs, main, gitIgnore
        );
        return restService;
    }

    private void setProjectDir(String projectDir) {
        this.projectDir = projectDir;
    }

    private void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    private void setRootPackage(String rootPackage) {
        this.rootPackage = rootPackage;
    }

    private void setBuildFile(BuildFile buildFile) {
        this.buildFile = buildFile;
    }

    private void setGitIgnore(GitIgnore gitIgnore) {
        this.gitIgnore = gitIgnore;
    }

    private void setSettingsFile(SettingsFile settingsFile) {
        this.settingsFile = settingsFile;
    }
}
