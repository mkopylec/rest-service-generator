package pl.allegro.tech.generator.domain.java;

import org.stringtemplate.v4.ST;

public class Main extends JavaClass {

    public Main(String name) {
        super(null, name, "main");
    }

    @Override
    protected void addAttributes(ST template) {

    }
}
