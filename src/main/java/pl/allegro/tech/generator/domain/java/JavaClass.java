package pl.allegro.tech.generator.domain.java;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.stringtemplate.v4.ST;
import pl.allegro.tech.generator.domain.SourceFile;

import java.util.HashSet;
import java.util.Set;

import static pl.allegro.tech.generator.domain.SourceFileType.JAVA;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfNull;

abstract class JavaClass extends SourceFile {

    private String fullPackage;
    private String rootPackage;
    private final String subPackage;
    private final Set<String> imports;
    private final String name;

    protected JavaClass(String subPackage, String name, String templateName) {
        super(templateName, JAVA);
        this.subPackage = subPackage;
        imports = new HashSet<>();
        this.name = name;
    }

    protected abstract void addAttributes(ST template);

    @Override
    protected final void fillTemplate(ST template) {
        template
                .add("package", fullPackage)
                .add("imports", imports)
                .add("name", name);
        addAttributes(template);
    }

    @Override
    protected final String getRelativeFilePath() {
        return new StringBuilder()
                .append(JAVA.getSourceFilesDir())
                .append("/")
                .append(fullPackage.replace(".", "/"))
                .append("/")
                .append(name)
                .append(JAVA.getFileExtension())
                .toString();
    }

    public void addImport(String classImport) {
        failIfBlank(classImport, "Import statement is empty");
        imports.add(classImport);
    }

    public void addImports(Set<String> classImports) {
        failIfNull(classImports, "Import statements are null");
        imports.addAll(classImports);
    }

    public Set<String> getImports() {
        return imports;
    }

    public String getName() {
        return name;
    }

    public String getSubPackage() {
        return subPackage;
    }

    public String getRootPackage() {
        return rootPackage;
    }

    public void setRootPackage(String rootPackage) {
        failIfBlank(rootPackage, "Java class root package is empty");
        this.rootPackage = rootPackage;
        fullPackage = subPackage != null
                ? rootPackage + "." + subPackage
                : rootPackage;
    }

    public String getFullPackage() {
        return fullPackage;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        JavaClass rhs = (JavaClass) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.fullPackage, rhs.fullPackage)
                .append(this.name, rhs.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(fullPackage)
                .append(name)
                .toHashCode();
    }
}
