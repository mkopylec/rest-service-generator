package pl.allegro.tech.generator.domain.java;

public class ClassPropertyBuilder {

    private String type;
    private String name;
    private boolean getter;
    private boolean setter;
    private boolean collection;

    private ClassPropertyBuilder() {
    }

    public static ClassPropertyBuilder newProperty() {
        return new ClassPropertyBuilder();
    }

    public ClassPropertyBuilder withType(String type) {
        this.type = type;
        return this;
    }

    public ClassPropertyBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ClassPropertyBuilder withGetter(boolean getter) {
        this.getter = getter;
        return this;
    }

    public ClassPropertyBuilder withSetter(boolean setter) {
        this.setter = setter;
        return this;
    }

    public ClassPropertyBuilder isCollection(boolean collection) {
        this.collection = collection;
        return this;
    }

    public ClassProperty build() {
        ClassProperty property = new ClassProperty(type, name, getter, setter, collection);
        return property;
    }
}
