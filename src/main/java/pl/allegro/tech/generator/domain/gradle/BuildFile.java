package pl.allegro.tech.generator.domain.gradle;

import org.stringtemplate.v4.ST;
import pl.allegro.tech.generator.domain.SourceFile;

import java.util.Set;

import static pl.allegro.tech.generator.domain.SourceFileType.GRADLE;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;

public class BuildFile extends SourceFile {

    private final String gradleVersion;
    private final String javaVersion;
    private final String mainClass;
    private final String artifactGroup;
    private final Set<Dependency> compileDependencies;
    private final Set<Dependency> testDependencies;

    BuildFile(String gradleVersion, String javaVersion, String mainClass, String artifactGroup, Set<Dependency> compileDependencies, Set<Dependency> testDependencies) {
        super("build", GRADLE);
        failIfBlank(gradleVersion, "Build file gradle version is empty");
        failIfBlank(javaVersion, "Build file java version is empty");
        failIfBlank(mainClass, "Build file main class is empty");
        failIfBlank(artifactGroup, "Build file artifact group is empty");
        this.gradleVersion = gradleVersion;
        this.javaVersion = javaVersion;
        this.mainClass = mainClass;
        this.artifactGroup = artifactGroup;
        this.compileDependencies = compileDependencies;
        this.testDependencies = testDependencies;
    }

    @Override
    protected final void fillTemplate(ST template) {
        template
                .add("gradleVersion", gradleVersion)
                .add("javaVersion", javaVersion)
                .add("mainClass", mainClass)
                .add("artifactGroup", artifactGroup)
                .add("compileDependencies", compileDependencies)
                .add("testDependencies", testDependencies);
    }

    @Override
    protected final String getRelativeFilePath() {
        return new StringBuilder()
                .append(GRADLE.getSourceFilesDir())
                .append("build")
                .append(GRADLE.getFileExtension())
                .toString();
    }

    public String getGradleVersion() {
        return gradleVersion;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public String getMainClass() {
        return mainClass;
    }

    public String getArtifactGroup() {
        return artifactGroup;
    }

    public Set<Dependency> getCompileDependencies() {
        return compileDependencies;
    }

    public Set<Dependency> getTestDependencies() {
        return testDependencies;
    }
}
