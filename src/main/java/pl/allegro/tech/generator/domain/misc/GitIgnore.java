package pl.allegro.tech.generator.domain.misc;

import org.stringtemplate.v4.ST;
import pl.allegro.tech.generator.domain.SourceFile;

import static pl.allegro.tech.generator.domain.SourceFileType.MISC;

public class GitIgnore extends SourceFile {

    public GitIgnore() {
        super("gitignore", MISC);
    }

    @Override
    protected void fillTemplate(ST template) {

    }

    @Override
    protected String getRelativeFilePath() {
        return new StringBuilder()
                .append(MISC.getSourceFilesDir())
                .append(".gitignore")
                .toString();
    }
}
