package pl.allegro.tech.generator.domain.gradle;

import org.stringtemplate.v4.ST;
import pl.allegro.tech.generator.domain.SourceFile;

import static pl.allegro.tech.generator.domain.SourceFileType.GRADLE;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;

public class SettingsFile extends SourceFile {

    private String projectName;

    public SettingsFile(String projectName) {
        super("settings", GRADLE);
        failIfBlank(projectName, "Project name is empty");
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    @Override
    protected void fillTemplate(ST template) {
        template
                .add("projectName", projectName);
    }

    @Override
    protected String getRelativeFilePath() {
        return new StringBuilder()
                .append(GRADLE.getSourceFilesDir())
                .append("settings")
                .append(GRADLE.getFileExtension())
                .toString();
    }
}
