package pl.allegro.tech.generator.domain.java;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static pl.allegro.tech.generator.utils.CheckUtils.failIfBlank;
import static pl.allegro.tech.generator.utils.PrimitivesWrapper.getWrapper;

public class ClassProperty {

    private final boolean primitive;
    private final String type;
    private String displayedType;
    private final String name;
    private final boolean getter;
    private final boolean setter;
    private final boolean collection;

    ClassProperty(String type, String name, boolean getter, boolean setter, boolean collection) {
        failIfBlank(type, "Property type is empty");
        failIfBlank(name, "Property name is empty");
        primitive = type.matches("[a-z]+");
        this.type = type;
        displayedType = type.substring(type.lastIndexOf(".") + 1);
        if (collection) {
            displayedType = format("List<%s>", primitive ? getWrapper(displayedType) : displayedType);
        }
        this.name = name;
        this.getter = getter;
        this.setter = setter;
        this.collection = collection;
    }

    public String getType() {
        return type;
    }

    public String getDisplayedType() {
        return displayedType;
    }

    public boolean isPrimitive() {
        return primitive;
    }

    public String getName() {
        return name;
    }

    public boolean hasGetter() {
        return getter;
    }

    public boolean hasSetter() {
        return setter;
    }

    public boolean isCollection() {
        return collection;
    }

    public Set<String> getImportsToAdd() {
        Set<String> imports = new HashSet<>();
        if (type.contains(".") && !type.startsWith("java.lang")) {
            imports.add(type);
        }
        if (collection) {
            imports.add("java.util.List");
        }
        return imports;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ClassProperty rhs = (ClassProperty) obj;
        return new EqualsBuilder()
                .append(this.name, rhs.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .toHashCode();
    }
}
