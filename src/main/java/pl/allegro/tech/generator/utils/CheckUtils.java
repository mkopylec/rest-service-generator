package pl.allegro.tech.generator.utils;

import pl.allegro.tech.generator.domain.EmptyValueException;

import static org.apache.commons.lang3.StringUtils.isBlank;

public final class CheckUtils {

    public static void failIfNull(Object value, String message) {
        if (value == null) {
            throw new EmptyValueException(message);
        }
    }

    public static void failIfBlank(String value, String message) {
        if (isBlank(value)) {
            throw new EmptyValueException(message);
        }
    }

    private CheckUtils() {
    }
}
