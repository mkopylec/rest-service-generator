package pl.allegro.tech.generator.utils;

import static java.lang.String.format;

public final class PrimitivesWrapper {

    public static String getWrapper(String primitiveType) {
        switch (primitiveType) {
            case "char":
                return "Character";
            case "byte":
                return "Byte";
            case "int":
                return "Integer";
            case "double":
                return "Double";
            case "boolean":
                return "Boolean";
            case "long":
                return "Long";
            default:
                throw new IllegalArgumentException(format("Unsupported primitive type: %s", primitiveType));
        }
    }

    private PrimitivesWrapper() {

    }
}
