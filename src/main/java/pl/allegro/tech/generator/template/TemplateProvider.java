package pl.allegro.tech.generator.template;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupDir;
import org.stringtemplate.v4.StringRenderer;
import pl.allegro.tech.generator.domain.SourceFileType;

import static pl.allegro.tech.generator.domain.SourceFileType.CONFIG;
import static pl.allegro.tech.generator.domain.SourceFileType.GRADLE;
import static pl.allegro.tech.generator.domain.SourceFileType.JAVA;
import static pl.allegro.tech.generator.domain.SourceFileType.MISC;

public final class TemplateProvider {

    private static final STGroupDir JAVA_TEMPLATES = new STGroupDir(JAVA.getTemplatesDir(), '$', '$');
    private static final STGroupDir GRADLE_TEMPLATES = new STGroupDir(GRADLE.getTemplatesDir(), '$', '$');
    private static final STGroupDir MISC_TEMPLATES = new STGroupDir(MISC.getTemplatesDir(), '$', '$');
    private static final STGroupDir CONFIG_TEMPLATES = new STGroupDir(CONFIG.getTemplatesDir(), '$', '$');

    static {
        JAVA_TEMPLATES.registerRenderer(String.class, new StringRenderer());
        GRADLE_TEMPLATES.registerRenderer(String.class, new StringRenderer());
    }

    public static ST getTemplate(String name, SourceFileType fileType) {
        switch (fileType) {
            case JAVA:
                return JAVA_TEMPLATES.getInstanceOf(name);
            case GRADLE:
                return GRADLE_TEMPLATES.getInstanceOf(name);
            case MISC:
                return MISC_TEMPLATES.getInstanceOf(name);
            case CONFIG:
                return CONFIG_TEMPLATES.getInstanceOf(name);
            default:
                return null;
        }
    }

    private TemplateProvider() {
    }
}
