package pl.allegro.tech.generator;

import pl.allegro.tech.generator.domain.RestService;
import pl.allegro.tech.generator.domain.config.ApplicationConfig;
import pl.allegro.tech.generator.domain.config.Property;
import pl.allegro.tech.generator.domain.gradle.BuildFile;
import pl.allegro.tech.generator.domain.gradle.Dependency;
import pl.allegro.tech.generator.domain.java.ClassProperty;
import pl.allegro.tech.generator.domain.java.Entity;
import pl.allegro.tech.generator.domain.java.Main;
import pl.allegro.tech.generator.domain.java.Repository;

import java.io.IOException;

import static pl.allegro.tech.generator.domain.RestServiceBuilder.newRestService;
import static pl.allegro.tech.generator.domain.gradle.BuildFileBuilder.newBuildFile;
import static pl.allegro.tech.generator.domain.java.ClassPropertyBuilder.newProperty;
import static pl.allegro.tech.generator.domain.java.EntityBuilder.newEntity;

public class Application {

    public static void main(String[] args) throws IOException {

        ClassProperty property = newProperty()
                .withName("created")
                .withType("java.util.Date")
                .withGetter(true)
                .withSetter(true)
                .build();
        ClassProperty property2 = newProperty()
                .withName("age")
                .withType("int")
                .withGetter(true)
                .withSetter(true)
                .isCollection(true)
                .build();
        ClassProperty property3 = newProperty()
                .withName("id")
                .withType("int")
                .withGetter(true)
                .withSetter(true)
                .build();
        ClassProperty property4 = newProperty()
                .withName("user")
                .withType("User")
                .withGetter(true)
                .withSetter(true)
                .build();

        Entity user = newEntity()
                .withName("User")
                .withAllPropsConstructor(true)
                .withEqualsAndHashCode(true)
                .withSubPackage("entities")
                .withPrimaryKey(property3)
                .build();
        Entity person = newEntity()
                .withName("Person")
                .withToString(true)
                .withSubPackage("entities")
                .withPrimaryKey(property3)
                .withProperty(property4)
                .withProperty(property2)
                .build();

        BuildFile buildFile = newBuildFile()
                .withArtifactGroup("pl.allegro.tech.generator")
                .withCompileDependency(new Dependency("org.glassfish.jersey.core", "jersey-client", "+"))
                .withCompileDependency(new Dependency("javax.inject", "javax.inject", "+"))
                .withTestDependency(new Dependency("junit", "junit", "+"))
                .withGradleVersion("2.2")
                .withJavaVersion("1.8")
                .withMainClass("pl.allegro.restservice.Application")
                .build();

        ApplicationConfig config = new ApplicationConfig();
        config.addProperty(new Property("test", "hahhaa"));
        config.addProperty(new Property("test2", "chuj"));
        ApplicationConfig config2 = new ApplicationConfig("dev");
        config2.addProperty(new Property("swsws", "hahhaa rfrfv"));
        config2.addProperty(new Property("aaassd", "chujddff"));
        config2.addProperty(new Property("aaassd", ""));
        config2.addProperty(new Property("aaassd", "xxxx"));

        RestService restService = newRestService("C:/Workspace/rest-service", "rest-service", "pl.allegro.restservice", buildFile)
                .withEntity(user)
                .withEntity(person)
                .withMainClass(new Main("Application"))
                .withRepository(new Repository("repositories", user))
                .withRepository(new Repository("repositories", person))
                .withApplicationConfig(config)
                .withApplicationConfig(config2)
                .build();

        restService.createProject();
    }
}
