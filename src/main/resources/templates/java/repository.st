repository(package, name, imports, entityType, primaryKeyType) ::= <<
package $package$;

$imports:{ import | import $import$;
}$import org.springframework.data.repository.CrudRepository;

public interface $name$ extends CrudRepository<$entityType$, $primaryKeyType$> {
}

>>
