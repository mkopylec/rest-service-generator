entity(package, imports, name, primaryKey, properties, noArgConstructor, allPropsConstructor, equalsAndHashCode, toString) ::= <<
package $package$;

import org.springframework.data.annotation.Id;
$imports:{ import | import $import$;
}$
public class $name$ {

    @Id
    private $primaryKey.displayedType$ $primaryKey.name$;
    $properties:{ property | private $property.displayedType$ $property.name$;
    }$
    $if(noArgConstructor)$
    public $name$() {
    }
    $endif$

    $if(allPropsConstructor)$
    public $name$($primaryKey.displayedType$ $primaryKey.name$$properties:{ property |  ,$property.displayedType$ $property.name$}$) {
        this.$primaryKey.name$ = $primaryKey.name$;$properties:{property | $\n$$\t$$\t$this.$property.name$ = $property.name$;}$
    }
    $endif$

    $if(primaryKey.getter)$
    public $primaryKey.displayedType$ get$primaryKey.name; format="cap"$() {
        return $primaryKey.name$;
    }
    $endif$

    $properties:{ property | $if(property.getter)$public $property.displayedType$ get$property.name; format="cap"$() {
    return $property.name$;
\}
    $endif$}$
    $properties:{ property | $if(property.setter)$public void set$property.name; format="cap"$($property.displayedType$ set$property.name$) {
    this.$property.name$ = $property.name$;
\}
    $endif$}$
    $if(equalsAndHashCode)$
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        $name$ other = ($name$) obj;
        return new EqualsBuilder()
                .append(this.$primaryKey.name$, other.$primaryKey.name$)
                $properties:{ property | .append(this.$property.name$, other.$property.name$)
                }$$\t$$\t$$\t$$\t$.isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append($primaryKey.name$)
                $properties:{ property | .append($property.name$)
                }$$\t$$\t$$\t$$\t$.toHashCode();
    }
    $endif$

    $if(toString)$
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("$primaryKey.name$", $primaryKey.name$)
                $properties:{ property | .append("$property.name$", $property.name$)
                }$$\t$$\t$$\t$$\t$.toString();
    }
    $endif$
}

>>
