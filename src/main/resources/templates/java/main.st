main(package, name, imports) ::= <<
package $package$;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class $name$ extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run($name$.class, args);
    }

    @Component
    public static class JerseyConfig extends ResourceConfig {

        public JerseyConfig() {
            //TODO Register your endpoints and providers:
            //register(Endpoint.class);
        }
    }
}

>>
