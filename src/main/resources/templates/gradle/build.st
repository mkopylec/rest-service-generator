build(gradleVersion, javaVersion, mainClass, artifactGroup, compileDependencies, testDependencies) ::= <<
buildscript {
    repositories {
        maven { url 'https://nexus.allegrogroup.com/nexus/content/repositories/allegro.pl.release'}
        mavenCentral()
        jcenter()
    }
    dependencies {
        classpath group: 'pl.allegro.tech.build', name: 'axion-gradle-plugin', version: '+'
        classpath group: 'pl.allegro.tech.build', name: 'axion-release-plugin', version: '+'
        classpath group: 'pl.allegro.tech.build', name: 'sonar-bamboo-plugin', version: '+'
    }
}

apply plugin: 'maven'
apply plugin: 'maven-publish'
apply plugin: 'application'
apply plugin: 'jacoco'

apply plugin: 'axion'
apply plugin: 'axion-release'
apply plugin: 'sonar-bamboo'

sourceCompatibility = '$javaVersion$'
[compileJava, compileTestJava]*.options*.encoding = 'UTF-8'

scmVersion {
    tag {
        prefix = 'v'
    }

    versionCreator 'versionWithBranch'
}

mainClassName = '$mainClass$'

project.group = '$artifactGroup$'
project.version = scmVersion.version

repositories {
    maven { url 'https://nexus.allegrogroup.com/nexus/content/repositories/allegro.pl.release' }
    mavenCentral()
}

dependencies {

    compile group: 'pl.allegro.tech.common', name: 'spring-boot-starter-andamio', version: '+'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-jersey', version: '1.2.0.RELEASE'
    compile group: 'org.glassfish.jersey.media', name: 'jersey-media-json-jackson', version: '+'
    compile group: 'org.glassfish.jersey.ext', name: 'jersey-bean-validation', version: '+'
    $compileDependencies:{ dependency | compile group: '$dependency.group$', name: '$dependency.name$', version: '$dependency.version$'
    }$
    testCompile group: 'org.springframework.boot', name: 'spring-boot-starter-test', version: '1.2.0.RELEASE'
    testCompile group: 'org.assertj', name: 'assertj-core', version: '+'
    $testDependencies:{ dependency | testCompile group: '$dependency.group$', name: '$dependency.name$', version: '$dependency.version$'}$
}

axionPublishing {
    applyDefaultPublication false

    repositories {
        myRepository {
            url = 'https://nexus.allegrogroup.com/nexus/content/repositories/allegro.pl.{}/'
        }
    }
    apply()
}

task wrapper(type: Wrapper) {
    gradleVersion = '$gradleVersion$'
}

>>
