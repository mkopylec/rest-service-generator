profile(properties, profile, projectName) ::= <<
application.name=$projectName$
application.environment=$if(profile)$$profile$$else$local$endif$

$properties:{ property | $property.name$=$property.value$
}$
>>
